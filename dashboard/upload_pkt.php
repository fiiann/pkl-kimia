
<?php 
	$site_name= "Pendaftaran PKT";
	require_once('sidebar.php');
	require_once('datediff.php');
	$id=$_SESSION['sip_masuk_aja'];
	if(($status=="dosen")||($status=="lab")){
		header('Location:./index.php');
	}
	$todayDate = date("Y-m-d");
	$db=new mysqli($db_host, $db_username, $db_password, $db_database);

	if($db->connect_errno){
		die("Could not connect to the database : <br/>". $db->connect_error);
	}

	$sukses=TRUE;
	$query = " SELECT awal,akhir FROM waktu WHERE id=1";
	// // Execute the query
	$result = $con->query( $query );
	if (!$result){
		die ("Could not query the database: <br />". $con->error);
	}
	else{
		while ($row = $result->fetch_object()){
			// $nim=$row->nim;
			$tgl_awal = $row->awal;
			$tgl_akhir = $row->akhir;
		}
	}
 ?>

<?php if (($tgl_awal < $todayDate)&&($todayDate < $tgl_akhir)): ?>
 	<!DOCTYPE html>
<html>
<head>
	<title>Form Pendaftaran</title>
</head>
<!-- $todayDate = date("Y-m-d");// current date -->
<body>

<div class="row">
	<div class="col-md-6">
		<!-- Form Elements -->
		<div class="panel panel-default">
			<div class="panel-heading">
				Daftar PKT
				<?php echo $keterangan; ?>

			</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<form method="POST" action="upload.php" method="post" enctype="multipart/form-data">
							<span class="label label-success"><?php if(isset($pesan_sukses)) echo $pesan_sukses;?></span>

							<!-- NIM -->
							<div class="form-group">
								<label>NIM</label>&nbsp;<span class="label label-warning">* <?php if(isset($errorNim)) echo $errorNim;?></span>
								<input class="form-control" type="text" name="nim" maxlength="14" size="30" <?php if ($status=="anggota") echo "readonly"; ?> placeholder="nim 14 digit angka" required autofocus value="<? if ($status=="anggota"){ echo $anggota->nim; }  ?>">
							</div>

							<div class="form-group">
								<label>Semester</label>&nbsp;<span class="label label-warning">* <?php if(isset($error_smt)) echo $error_smt;?></span>
								<input class="form-control" type="number" name="smt" min="1" max="12"  placeholder="numerik" required autofocus>
							</div>
							<div class="form-group">
								<label>Periode</label>&nbsp;<span class="label label-warning">* <?php if(isset($error_periode)) echo $error_smt;?></span>
								<select class="form-control" name="periode">
									<option value="16/17">2016/2017</option>
									<option value="17/18">2017/2018</option>
									<option value="18/19">2018/2019</option>
									<option value="19/20">2019/2020</option>
								</select>
							</div>
							<div class="form-group">
								<label>Pilih Laboratorium</label>&nbsp;
							</div>
							<!-- pilihan1 -->
							<div class="form-group">
								<label>Pilihan 1</label>&nbsp;<span class="label label-warning">* <?php if(isset($error_pilihan1)) echo $error_pilihan1;?></span>&nbsp;
								<select id="pilihan1" name="pilihan1" required>
							<option value="none">--Pilih lab 1--</option>
							<?php
								$querykat = "select * from lab";
								$resultkat = $db->query($querykat);
								if(!$resultkat){
									die("Could not connect to the database : <br/>". $db->connect_error);
								}
								while ($row = $resultkat->fetch_object()){
									$kid = $row->idlab;
									$kname = $row->nama_lab;
									echo "<option value=".$kid.' ';
									if(isset($pilihan1) && $pilihan1==$kid)
									echo "selected='true'";
									echo ">".$kname."<br/></option>";
								}
							?></select>
						<span class="error"> <?php if(!empty($error_pilihan1)) echo $error_pilihan1; ?></span>
										</div>

										<!-- pilihan2 -->
										<div class="form-group">
											<label>Pilihan 2</label>&nbsp;<span class="label label-warning">* <?php if(isset($error_pilihan2)) echo $error_pilihan2;?></span>&nbsp;
											<select id="pilihan2" name="pilihan2" required>
							<option value="none">--Pilih lab 2--</option>
							<?php
								$querykat = "select * from lab";
								$resultkat = $db->query($querykat);
								if(!$resultkat){
									die("Could not connect to the database : <br/>". $db->connect_error);
								}
								while ($row = $resultkat->fetch_object()){
									$sid = $row->idlab;
									$sname = $row->nama_lab;
									echo "<option value=".$sid.' ';
									if(isset($pilihan2) && $pilihan2==$sid)
									echo "selected='true'";
									echo ">".$sname."<br/></option>";
								}
							?></select>
							</div>
										<!-- pilihan3 -->
							<div class="form-group">
							<label>Pilihan 3</label>&nbsp;<span class="label label-warning">* <?php if(isset($error_pilihan3)) echo $error_pilihan3;?></span>&nbsp;
							<select id="pilihan3" name="pilihan3" required>
							<option value="none">--Pilih lab 3--</option>
							<?php
								$querykat = "select * from lab";
								$resultkat = $db->query($querykat);
								if(!$resultkat){
									die("Could not connect to the database : <br/>". $db->connect_error);
								}
								while ($row = $resultkat->fetch_object()){
									$tid = $row->idlab;
									$tname = $row->nama_lab;
									echo "<option value=".$tid.' ';
									if(isset($pilihan3) && $pilihan3==$tid)
									echo 'selected="true"';
									echo ">".$tname."<br/></option>";
									echo "cek";
								}
							?></select>
			<!-- <span class="error"> <?php if(!empty($error_pilihan3)) echo $error_pilihan3; ?></span> -->
							</div>
							Select krs to upload:
    						<input type="file" name="fileToUpload" id="fileToUpload">
    						<br>
    						Select khs to upload:
    						<input type="file" name="khs" id="khs">
    						<br>
    						Select transkrip to upload:
    						<input type="file" name="transkrip" id="transkrip">
    						<br>
    						Select transkrip lengkap to upload:
    						<input type="file" name="transkrip_lengkap" id="transkrip_lengkap">
    						<br>

							<div class="form-group">

								 <input class="form-control" type="submit"  name="daftar" value="Daftar">
							</div>
						</form>
					</div>

				</div>
			</div>

		</div>
	</div>
</div>
 <?php else: ?>
 	<div class="col-lg-12">
		<h1>Diluar Batas Waktu Pendaftaran</h1>
	</div>
 <?php endif ?>

<?php
include_once('footer.php');
$con->close();
?>



<!-- if -->

