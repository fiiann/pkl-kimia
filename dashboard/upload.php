
<?php
require_once('sidebar.php');
$db=new mysqli($db_host, $db_username, $db_password, $db_database);

    if($db->connect_errno){
        die("Could not connect to the database : <br/>". $db->connect_error);
    }

$target_dir_krs = "testupload/krs/";
$target_dir_khs = "testupload/khs/";
$target_dir_transkrip = "testupload/transkrip/";
$target_dir_lengkap = "testupload/transkrip_lengkap/";

$target_file = $target_dir_krs . basename($_FILES["fileToUpload"]["name"]);
$target_file_khs = $target_dir_khs . basename($_FILES["khs"]["name"]);
$target_file_transkrip = $target_dir_transkrip . basename($_FILES["transkrip"]["name"]);
$target_file_transkrip_lengkap = $target_dir_lengkap . basename($_FILES["transkrip_lengkap"]["name"]);

$uploadOk = 1;
$imageFileType_krs = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$imageFileType_khs = strtolower(pathinfo($target_file_khs,PATHINFO_EXTENSION));
$imageFileType_transkrip = strtolower(pathinfo($target_file_transkrip,PATHINFO_EXTENSION));
$imageFileType_transkrip_lengkap = strtolower(pathinfo($target_file_transkrip_lengkap,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
// if(isset($_POST["submit"])) {
//     $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
//     if($check !== false) {
//         echo "File is an image - " . $check["mime"] . ".";
//         $uploadOk = 1;
//     } else {
//         echo "File is not an image.";
//         $uploadOk = 0;
//     }
// }
// Check if file already exists
if (file_exists($target_file)||file_exists($target_file_khs)||file_exists($target_file_transkrip)||file_exists($target_file_transkrip_lengkap)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 5000000||$_FILES["khs"]["size"] > 5000000||$_FILES["transkrip"]["size"] > 5000000||$_FILES["transkrip_lengkap"]["size"] > 5000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType_krs != "jpg" && $imageFileType_krs != "png" && $imageFileType_krs != "jpeg"
&& $imageFileType_krs != "pdf" && $imageFileType_khs != "jpg" && $imageFileType_khs != "png" && $imageFileType_khs != "jpeg"
&& $imageFileType_khs != "pdf" && $imageFileType_transkrip != "jpg" && $imageFileType_transkrip != "png" && $imageFileType_transkrip != "jpeg"
&& $imageFileType_transkrip != "pdf" && $imageFileType_transkrip_lengkap != "jpg" && $imageFileType_transkrip_lengkap != "png" && $imageFileType_transkrip_lengkap != "jpeg"
&& $imageFileType_transkrip_lengkap != "pdf") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    $nim=test_input($_POST['nim']);
       // $nim='24010314120044';
            $querys = " SELECT * FROM mahasiswa WHERE nim='".$nim."'";
            $results = $con->query( $querys );

        if ($nim=='') {
            $errorNim='wajib diisi';
            $validNim=FALSE;
        }elseif (!preg_match("/^[0-9]{14}$/",$nim)) {
            $errorNim='NIM harus terdiri dari 14 digit angka';
            $validNim=FALSE;
        }
        // elseif ($results->num_rows!=0) {
        //      $errorNim="NIM belum terdaftar di sistem";
        //      $validNim=FALSE;
        // }
        else{
            $query = " SELECT * FROM pkt WHERE nim='".$nim."'";
            $result = $con->query( $query );
            if($result->num_rows!=0){
                $errorNim="NIM sudah pernah digunakan, harap masukkan NIM lain";
                $validNim=FALSE;
            }
            else{
                $validNim = TRUE;
            }
        }

        $smt=$_POST['smt'];
        $smt = test_input($_POST['smt']);
        if($smt == '' || $smt == "none"){
            $error_smt= "Laboratorium harus diisi";
            $valid_smt= FALSE;
        } else{
            $valid_smt= TRUE;
        }

        $pilihan1=$_POST['pilihan1'];
        $pilihan2=$_POST['pilihan2'];
        $pilihan3=$_POST['pilihan3'];
        $pilihan1 = test_input($_POST['pilihan1']);
        if($pilihan1 == '' || $pilihan1 == "none"){
            $error_pilihan1= "Laboratorium harus diisi";
            $valid_pilihan1= FALSE;
        } else{
            $valid_pilihan1= TRUE;
        }

        $pilihan2 = test_input($_POST['pilihan2']);
        if($pilihan2 == '' || $pilihan2 == "none"){
            $error_pilihan2= "Laboratorium harus diisi";
            $valid_pilihan2= FALSE;
        } elseif (($pilihan1==$pilihan2)||($pilihan2==$pilihan3)) {
            $error_pilihan2 = "Pilihan lab harus berbeda";
            $valid_pilihan2 = FALSE;
        }else{
            $valid_pilihan2= TRUE;
        }


        $pilihan3 = test_input($_POST['pilihan3']);
        if($pilihan3 == '' || $pilihan3 == "none"){
            $error_pilihan3= "Laboratorium harus diisi";
            $valid_pilihan3= FALSE;
        }elseif ($pilihan1==$pilihan3) {
            $error_pilihan3 = "Pilihan lab harus berbeda";
            $valid_pilihan3=FALSE;
        } else{
            $valid_pilihan3= TRUE;
        }
        $periode=$_POST['periode'];

    if ((move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) && (move_uploaded_file($_FILES["khs"]["tmp_name"], $target_file_khs)) && (move_uploaded_file($_FILES["transkrip"]["tmp_name"], $target_file_transkrip)) && (move_uploaded_file($_FILES["transkrip_lengkap"]["tmp_name"], $target_file_transkrip_lengkap)) && $validNim && $valid_pilihan1 && $valid_pilihan2 && $valid_pilihan3 && $valid_smt) {
        // echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        


        // jika tidak ada kesalahan input
        // if ($validNim && $valid_pilihan1 && $valid_pilihan2 && $valid_pilihan3 && $valid_smt) {
            $nim=$con->real_escape_string($nim);
            $smt=$con->real_escape_string($smt);
            $pilihan1=$con->real_escape_string($pilihan1);
            $pilihan2=$con->real_escape_string($pilihan2);
            $pilihan3=$con->real_escape_string($pilihan3);

            $query = "INSERT INTO pkt (nim, pilihan_lab1, pilihan_lab2, pilihan_lab3,smt,periode,upload_krs,upload_khs,upload_transkrip,upload_transkrip_lengkap) VALUES ('".$nim."','".$pilihan1."','".$pilihan2."','".$pilihan3."','".$smt."','".$periode."','".$target_file."','".$target_file_khs."','".$target_file_transkrip."','".$target_file_transkrip_lengkap."')";
            $hasil=$con->query($query);

            $que = "SELECT id_pkt from pkt WHERE nim=$nim";
            $hasilq=$con->query($que);
            $rows = $hasilq->fetch_object();
            $id = $rows->id_pkt;

            $query1 = "INSERT INTO nilai_pkt (id_pkt,id_komponen) VALUES ('".$id."',1),('".$id."',2),('".$id."',3)" ;

            $hasil1=$con->query($query1);
            if (!($hasil && $hasil1)) {
                die("Tidak dapat menjalankan query database: <br>".$con->error);
            }else{
                $sukses=TRUE;
            }
            echo $pesan_sukses="Berhasil mendaftar PKT.";
        // }
        // else{
        // }
    } else {
            $sukses=FALSE;
        echo "Sorry, there was an error uploading your fianfile.";
    }
}
?>


