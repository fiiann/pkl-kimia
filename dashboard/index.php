﻿<?php
	include_once('sidebar.php');
	if($status=='petugas'){
			$pesanWelcome='""';
			$query1="SELECT count(nim) as counter FROM pkt";
			$query_pkt="SELECT awal,akhir FROM waktu WHERE id=1";
			$query_tr1="SELECT awal,akhir FROM waktu WHERE id=2";
			$result_pkt = $con->query($query_pkt);
			$row_pkt=$result_pkt->fetch_object();
			$pkt_awal=$row_pkt->awal;
			$pkt_akhir=$row_pkt->akhir;
			$result_tr1 = $con->query($query_tr1);
			$row_tr1=$result_tr1->fetch_object();
			$tr1_awal=$row_tr1->awal;
			$tr1_akhir=$row_tr1->akhir;
	}elseif($status=='dosen'){
			$pesanWelcome='Selamat datang Dosen Kimia';
			$query1 = "SELECT count(nim) as counter FROM pkt WHERE pkt.dosen_pembimbing='".$dosen->nip."'";
	}elseif ($status=='lab') {
			$query1="SELECT count(nim) as counter FROM pkt p WHERE p.flag_lab='".$lab->idlab."'";
	}else {
		$query1="SELECT count(nim) as counter FROM pkt";
		$pesanWelcome='"Sukses PKT & TR nya"';
	}

// batas waktu pkt
	if ($status=='petugas') {
		require_once('datediff.php');
		$db=new mysqli($db_host, $db_username, $db_password, $db_database);
	if($db->connect_errno){
		die("Could not connect to the database : <br/>". $db->connect_error);
	}

	$sukses=TRUE;

	// eksekusi tombol daftar
	if (isset($_POST['pkt'])||isset($_POST['tr1'])) {
		// Cek Nim


		$tgl_awal=$_POST['tgl_awal'];
		$tgl_awal = test_input($_POST['tgl_awal']);
		if($tgl_awal == '' || $tgl_awal == "none"){
			$error_tgl_awal= "Tanggal harus diisi";
			$valid_tgl_awal= FALSE;
		} else{
			$valid_tgl_awal= TRUE;
		}

		$tgl_akhir=$_POST['tgl_akhir'];
		$tgl_akhir = test_input($_POST['tgl_akhir']);
		if($tgl_akhir == '' || $tgl_akhir == "none"){
			$error_tgl_akhir= "Tanggal harus diisi";
			$valid_tgl_akhir= FALSE;
		} else{
			$valid_tgl_akhir= TRUE;
		}

		

		// jika tidak ada kesalahan input
		if ($valid_tgl_awal && $valid_tgl_akhir) {
			$tgl_awal=$con->real_escape_string($tgl_awal);
			$tgl_akhir=$con->real_escape_string($tgl_akhir);


			// $query = "UPDATE waktu SET awal='".$tgl_awal."',akhir='".$tgl_akhir."' WHERE id=1";
			if (isset($_POST['pkt'])) {
			$query = "UPDATE waktu SET awal='".$tgl_awal."',akhir='".$tgl_akhir."' WHERE id=1";
			$hasil=$con->query($query);

				if (!($hasil)) {
					die("Tidak dapat menjalankan query database: <br>".$con->error);
				}else{
					$sukses=TRUE;
				}
				$pesan_sukses="Berhasil menambahkan data.";
			}else {
			$query = "UPDATE waktu SET awal='".$tgl_awal."',akhir='".$tgl_akhir."' WHERE id=2";
				if (!($hasil)) {
					die("Tidak dapat menjalankan query database: <br>".$con->error);
				}else{
					$sukses=TRUE;
				}
				$pesan_sukses1="Berhasil menambahkan data.";
			}

			
		}
		else{
			$sukses=FALSE;
		}
	}
	}


	$result1 = $con->query($query1);
	$row=$result1->fetch_object();
	$jml_pkt=$row->counter;
	//
	$query="SELECT count(nim) as counter FROM tr1";
	$result = $con->query($query);
	$row=$result->fetch_object();
	$jml_tr1=$row->counter;
	//
	$query="SELECT count(nim) as counter FROM mahasiswa";
	$result = $con->query($query);
	$row=$result->fetch_object();
	$jml_anggota=$row->counter;
	// $query="SELECT count(idtransaksi) as counter FROM detail_transaksi WHERE tgl_kembali='0000-00-00'";
?>
<div class="row">
    <div class="col-md-12">
        <h2>Dashboard</h2>

        <h5>Selamat datang <b><?php if($status=="petugas") echo "Admin ".$petugas->nama; elseif($status=="dosen") echo "Dosen ".$dosen->nama_dosen;elseif($status=="lab") echo "Admin Lab ".$lab->nama_lab; else echo "Mahasiswa ".$anggota->nama; ?></b>. <small><i><?php echo $pesanWelcome ?></i></small></h5>
    </div>
</div><hr />
 <div class="row">
					<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="panel panel-back noti-box">
							<span class="icon-box bg-color-green set-icon">
								<i class="fa fa-book"></i>
							</span>
							<div class="text-box" >
								<div class="main-text"><?php echo $jml_pkt ?></div>
								<div class="text-muted">Mahasiswa PKT</div>
							</div>
						</div>
					</div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
						<div class="panel panel-back noti-box">
							<span class="icon-box bg-color-blue set-icon">
								<i class="fa fa-book"></i>
							</span>
							<div class="text-box" >

								<div class="main-text"><?php if(($status=='anggota')||($status=='lab')) echo $jml_tr1;else echo $jml_tr1; ?></div>
								<div class="text-muted"><?php if($status=='anggota') echo 'Mahasiswa TR1'; else echo 'Mahasiswa TR1'; ?></div>
							</div>
						 </div>
					</div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
						<!-- <div class="panel panel-back noti-box">
							<span class="icon-box bg-color-red set-icon">
								<?php if($status=='anggota') echo '<i class="fa fa-book"></i>'; else echo '<i class="fa fa-users"></i>'; ?>
							</span>
							<div class="text-box" >
								<div class="main-text"><?php if(($status=='anggota')||($status=='lab')) echo $jml_tr1;else echo $jml_tr1; ?></div>
								<div class="text-muted"><?php if($status=='anggota') echo 'Mahasiswa TR 2'; elseif ($status=='dosen') echo 'z'; else echo 'Anggota'; ?></div>
							</div>
						 </div> -->
					</div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
					</div>
				</div>

				<hr />
				<?php if ($status=='petugas'): ?>
					<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<h5><b>Atur Tanggal Pendaftaran PKT</b></h5>
						<form method="POST" role="form" autocomplete="on" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
							<span class="label label-success"><?php if(isset($pesan_sukses)) echo $pesan_sukses;?></span>

							<!-- NIM -->
							<div class="form-group">
								<label>Tanggal Awal</label>&nbsp;<span class="label label-warning">* <?php if(isset($error_tgl_awal)) echo $error_tgl_awal;?></span>
								<input class="form-control" type="date" name="tgl_awal" value="<?php if(isset($pkt_awal)){
									echo $pkt_awal;
								} ?>" maxlength="14" size="30" placeholder="tanggal" required autofocus >
							</div>
<?php $retVal = (condition) ? a : b; ?>

							<div class="form-group">
								<label>Tanggal Akhir</label>&nbsp;<span class="label label-warning">* <?php if(isset($error_tgl_akhir)) echo $error_tgl_akhir;?></span>
								<input class="form-control" type="date" name="tgl_akhir" maxlength="14" size="30"  placeholder="tanggal" value="<?php if(isset($pkt_akhir)){
									echo $pkt_akhir;
								} ?>" required autofocus>
							</div>

							<div class="form-group">

								 <input class="form-control" type="submit"  name="pkt" value="Atur">
							</div>
						</form>
					</div>
					<div class="col-md-6">
						<h5><b>Atur Tanggal Pendaftaran TR1</b></h5>
						<form method="POST" role="form" autocomplete="on" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
							<span class="label label-success"><?php if(isset($pesan_sukses1)) echo $pesan_sukses1;?></span>

							<!-- NIM -->
							<div class="form-group">
								<label>Tanggal Awal</label>&nbsp;<span class="label label-warning">* <?php if(isset($error_tgl_awal)) echo $error_tgl_awal;?></span>
								<input class="form-control" type="date" name="tgl_awal" maxlength="14" size="30" placeholder="tanggal" value="<?php if(isset($tr1_awal)){
									echo $tr1_awal;
								} ?>" required autofocus >
							</div>


							<div class="form-group">
								<label>Tanggal Akhir</label>&nbsp;<span class="label label-warning">* <?php if(isset($error_tgl_akhir)) echo $error_tgl_akhir;?></span>
								<input class="form-control" type="date" name="tgl_akhir" maxlength="14" size="30"  placeholder="tanggal" value="<?php if(isset($tr1_akhir)){
									echo $tr1_akhir;
								} ?>" required autofocus>
							</div>

							<div class="form-group">

								 <input class="form-control" type="submit"  name="tr1" value="Atur">
							</div>
						</form>
					</

				</div>
			</div>
				<?php endif ?>
              	

		</div>
<?php 
	include_once("footer.php");
	mysqli_close($con);
?>