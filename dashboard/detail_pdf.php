<!--
	Tanggal		: 25 November 2016
	Program		: pendaftaran_petugas.php
	Deskripsi	: menambah data anggota pada database
-->
<?php
	require_once('sidebar.php');
	

	$db=new mysqli($db_host, $db_username, $db_password, $db_database);

	if($db->connect_errno){
		die("Could not connect to the database : <br/>". $db->connect_error);
	}

	if($status!='petugas'){
		header('Location:./index.php');
	}


	$sukses=TRUE;
	$id_pkt=$_GET['id'];
	

			$query = "SELECT upload_krs,upload_khs,upload_transkrip,upload_transkrip_lengkap FROM pkt WHERE pkt.id_pkt='".$id_pkt."'";

			$result = $con->query( $query );
			if (!$result){
				die ("Could not query the database: <br />". $con->error);
			}else{
				while ($row = $result->fetch_object()){
					$krs=$row->upload_krs;
					$khs = $row->upload_khs;
					$transkrip = $row->upload_transkrip;
					$transkrip_lengkap = $row->upload_transkrip_lengkap;
			}
		}
?>
<div class="row">
	<div class="col-md-6">
		<!-- Form Elements -->
		<div class="panel panel-default">
			<div class="panel-heading">
				PDF 
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<br>
						<?php 
							 $a = ' <a href="';
					            $b = "http://localhost/pkl/pkl-kimia/dashboard/".$krs;
					            $c = '" target="_blank">KRS</a> ';
					            $link = $a.$b.$c;
					            $a = ' <a href="';
					            $b = "http://localhost/pkl/pkl-kimia/dashboard/".$khs;
					            $c = '" target="_blank">KHS</a> ';
					            $link2 = $a.$b.$c;
					            $a = ' <a href="';
					            $b = "http://localhost/pkl/pkl-kimia/dashboard/".$transkrip;
					            $c = '" target="_blank">Transkrip</a> ';
					            $link3 = $a.$b.$c;
					            $a = ' <a href="';
					            $b = "http://localhost/pkl/pkl-kimia/dashboard/".$transkrip_lengkap;
					            $c = '" target="_blank">Transkrip lengkap</a> ';
					            $link4 = $a.$b.$c;
					            echo 'KRS :'.$link.'<br>';
					            echo 'KHS :'.$link2.'<br>';
					            echo 'TRANSKRIP :'.$link3.'<br>';
					            echo 'TRANSKRIP LENGKAP :'.$link4.'<br>';
						 ?>
					</div>
				</div>
			</div>
		</div>
	&nbsp;&nbsp;&nbsp;<a href="kelola_mhs_pkt.php"><button class="btn btn-info">Kembali ke Kelola PKT</button></a>
	</div>
</div>

<?php
include_once('footer.php');
$con->close();
?>
